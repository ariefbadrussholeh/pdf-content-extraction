﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System;
using Tabula.Detectors;
using Tabula.Extractors;
using Tabula;
using UglyToad.PdfPig.Core;
using UglyToad.PdfPig;

namespace pdf_content_extraction.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PDFController : ControllerBase
    {
        public string pdfFilePath = @"Storage\TestFile.pdf";


        [HttpGet("GetContent")]
        public Header GetContent()
        {
            using (PdfDocument pdf = PdfDocument.Open(pdfFilePath, new ParsingOptions() { ClipPaths = true }))
            {

                ObjectExtractor oe = new ObjectExtractor(pdf);
                PageArea page = oe.Extract(1);

                IExtractionAlgorithm ea = new BasicExtractionAlgorithm();

                var packetInfoRegion1 = new PdfRectangle(0, 493, 460, 288);
                List<Table> tables1 = ea.Extract(page.GetArea(packetInfoRegion1));
                var packetInfo1 = tables1[0].Rows;

                var packetInfoRegion2 = new PdfRectangle(460, 493, 840, 288);
                List<Table> tables2 = ea.Extract(page.GetArea(packetInfoRegion2));
                var packetInfo2 = tables2[0].Rows;

                return new Header
                {
                    ProductShowcase = packetInfo1[0][2].GetText(),
                    PacketId = packetInfo1[1][2].GetText(),
                    PacketType = packetInfo1[2][2].GetText(),
                    PacketName = packetInfo1[3][2].GetText(),
                    Institution = packetInfo1[4][2].GetText(),
                    Unit = packetInfo1[5][2].GetText(),
                    UnitAddress = packetInfo1[6][2].GetText(),
                    NPWP = packetInfo1[7][2].GetText(),
                    FundingSource = packetInfo1[8][2].GetText(),
                    CreatedAt = DateTime.ParseExact(packetInfo2[0][2].GetText(), "d MMMM", System.Globalization.CultureInfo.InvariantCulture),
                    UpdatedAt = DateTime.ParseExact(packetInfo2[1][2].GetText(), "d MMMM", System.Globalization.CultureInfo.InvariantCulture),
                    NumOfProduct = int.Parse(packetInfo2[2][2].GetText()),
                    TotalPrice = float.Parse(packetInfo2[3][2].GetText().Replace("Rp ", "").Replace(".", "").Replace(",00", "")),
                    Currency = packetInfo2[7][2].GetText(),
                    ExchangeRate = float.Parse(packetInfo2[8][2].GetText()),
                    CurrencyDate = DateTime.ParseExact(packetInfo2[9][2].GetText(), "d-M-yyyy", System.Globalization.CultureInfo.InvariantCulture),
                    FiscalYear = packetInfo2[10][2].GetText(),
                    Items = GetItems()
                };

            }
        }

        [HttpGet("GetItems")]
        public List<Item> GetItems()
        {
            using (PdfDocument pdf = PdfDocument.Open(pdfFilePath, new ParsingOptions() { ClipPaths = true }))
            {
                // Create a list to store all items
                List<Item> itemsList = new List<Item>();

                // Iterate through pages
                for (int pageIndex = 2; pageIndex <= pdf.NumberOfPages; pageIndex++)
                {
                    ObjectExtractor oe = new ObjectExtractor(pdf);
                    PageArea page = oe.Extract(pageIndex);

                    // detect candidate table zones
                    SimpleNurminenDetectionAlgorithm detector = new SimpleNurminenDetectionAlgorithm();
                    var regions = detector.Detect(page);

                    IExtractionAlgorithm ea = new BasicExtractionAlgorithm();

                    // Iterate through candidate areas
                    foreach (var region in regions)
                    {
                        List<Table> tables = ea.Extract(page.GetArea(region.BoundingBox));

                        // Iterate through tables
                        foreach (var table in tables)
                        {
                            var rows = table.Rows;

                            var items = new Item
                            {
                                Name = rows[0][1].GetText(),
                                Manufacture = rows[1][1].GetText(),
                                Quantity = int.Parse(rows[2][1].GetText().Replace(".00", "")),
                                UnitPrice = float.Parse(rows[3][1].GetText().Replace("Rp ", "").Replace(".", "").Replace(",00", "")),
                                DeliveryDate = DateTime.ParseExact(rows[4][1].GetText(), "d-M-yyyy (d MMMM yyyy)", System.Globalization.CultureInfo.InvariantCulture),
                                TotalPrice = float.Parse(rows[5][1].GetText().Replace("Rp ", "").Replace(".", "").Replace(",00", "")),
                                Note = rows[6][1].GetText()
                            };

                            // Add the item to the list
                            itemsList.Add(items);
                        }
                    }
                }

                return itemsList;
            }
        }
    }

    public class Header
    {
        public string ProductShowcase { get; set; }
        public string PacketId { get; set; }
        public string PacketType { get; set; }
        public string PacketName { get; set; }
        public string Institution { get; set; }
        public string Unit { get; set; }
        public string UnitAddress { get; set; }
        public string NPWP { get; set; }
        public string FundingSource { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int NumOfProduct { get; set; }
        public float TotalPrice { get; set; }
        public string Currency { get; set; }
        public float ExchangeRate { get; set; }
        public DateTime CurrencyDate { get; set; }
        public string FiscalYear { get; set; }
        public List<Item> Items { get; set; }

    }

    public class Item
    {
        public string Name { get; set; }
        public string Manufacture { get; set; }
        public int Quantity { get; set; }
        public float UnitPrice { get; set; }
        public DateTime DeliveryDate { get; set; }
        public float TotalPrice { get; set; }
        public string Note { get; set; }
    }
}
